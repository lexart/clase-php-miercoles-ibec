<?php 

	$ruteo = "celulares";
	$rutas = array(
				array("ruta" => "celulares", "class" => "Celular"),
				array("ruta" => "celular_post", "class" => "Celular"),
				array("ruta" => "celular_edit", "class" => "Celular"),
				array("ruta" => "celulares_grid", "class" => "Celular"),
				array("ruta" => "celular_by_id", "class" => "Celular"),
				array("ruta" => "menus", "class" => "Menu"),

				array("ruta" => "usuarios_grid", "class" => "Usuario"),
				array("ruta" => "login", "class" => "Usuario"),
				array("ruta" => "find_user", "class" => "Usuario"),
			);
	$exist = false;

	if(isset($_GET) && !empty($_GET['ruteo'])){
		$ruteo = $_GET['ruteo'];

		foreach ($rutas as $key => $value) {
			if($value["ruta"] == $ruteo){
				$exist = true;
				$class = $value["class"];
				break;
			}
		}
	}


?>