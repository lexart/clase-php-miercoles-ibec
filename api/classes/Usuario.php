<?php 
	Class Usuario {

		public function getAllUsuariosItemsGrid($conn){
			$sql = "SELECT * FROM usuarios";
			$res = $conn->query($sql);

			return $res;
		}

		public function findUserByName($conn,$name){
			$sql = "SELECT * FROM usuarios WHERE nombre LIKE '%$name%'";
			$res = $conn->query($sql);

			return $res;
		}

		public function login($conn,$email,$clave){
			$sql = "SELECT * FROM usuarios WHERE email='$email' AND clave='$clave'";
			$res = $conn->query($sql);

			if(!empty($res)){
				return array("success" =>  array("token" => md5($res[0]["email"].$res[0]["clave"])) );
			} else {
				return array("err" => "Error al ingresar el email y/o clave.");
			}
			
		}

		public function insertNewCelular($conn,$celular){
			$sql = "INSERT INTO celulares (marca,modelo,precio) VALUES ('$celular[marca]', '$celular[modelo]', '$celular[precio]')";
			$res = $conn->query($sql);

			if(empty($res)){
				return array("success" => "Celular ".$celular["modelo"]." ingresado correctamente.");
			} else {
				return array("err" => "Error al ingresar el celular.");
			}
			
		}

		public function updateCelularById($conn,$celular){
			$sql = "UPDATE celulares SET marca = '$celular[marca]', modelo = '$celular[modelo]', precio = '$celular[precio]' WHERE id='$celular[id]'";
			$res = $conn->query($sql);

			if(empty($res)){
				return array("success" => "Celular ".$celular["modelo"]." actuializado correctamente.");
			} else {
				return array("err" => "Error al actualizar el celular.");
			}
		}
	}
?>