<?php 
	Class Celular {

		public function getAllCelularItems($conn){
			$sql = "SELECT * FROM celulares WHERE publicado = 1";
			$res = $conn->query($sql);

			return $res;
		}

		public function getAllCelularItemsGrid($conn){
			$sql = "SELECT * FROM celulares";
			$res = $conn->query($sql);

			return $res;
		}

		public function getCelularById($conn,$id){
			$sql = "SELECT * FROM celulares WHERE id='$id'";
			$res = $conn->query($sql);

			return $res[0];
		}

		public function insertNewCelular($conn,$celular){
			$sql = "INSERT INTO celulares (marca,modelo,precio) VALUES ('$celular[marca]', '$celular[modelo]', '$celular[precio]')";
			$res = $conn->query($sql);

			if(empty($res)){
				return array("success" => "Celular ".$celular["modelo"]." ingresado correctamente.");
			} else {
				return array("err" => "Error al ingresar el celular.");
			}
			
		}

		public function updateCelularById($conn,$celular){
			$sql = "UPDATE celulares SET marca = '$celular[marca]', modelo = '$celular[modelo]', precio = '$celular[precio]' WHERE id='$celular[id]'";
			$res = $conn->query($sql);

			if(empty($res)){
				return array("success" => "Celular ".$celular["modelo"]." actuializado correctamente.");
			} else {
				return array("err" => "Error al actualizar el celular.");
			}
		}
	}
?>