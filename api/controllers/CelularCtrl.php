<?php 
	$objCelular = new Celular();
	$results 	= array();

	if($ruteo == "celulares"){
		$results 	= $objCelular->getAllCelularItems($conn);
	}

	if($ruteo == "celulares_grid"){
		$results 	= $objCelular->getAllCelularItemsGrid($conn);
	}

	if($ruteo == "celular_by_id"){
		$id = 0;
		if(isset($_GET) && !empty($_GET['id'])){
			$id = $_GET['id'];
		}
		if($id){
			$results 	= $objCelular->getCelularById($conn,$id);
			if(empty($results)){
				$results = array("err" => "not found");
			}
		} else {
			$results 	= array("err" => "not found");
		}
	}

	if($ruteo == "celular_post"){
		$post 		= json_decode(file_get_contents('php://input'), true);
		$results 	= $objCelular->insertNewCelular($conn, $post);
	}

	if($ruteo == "celular_edit"){
		$post 		= json_decode(file_get_contents('php://input'), true);
		$results 	= $objCelular->updateCelularById($conn, $post);
	}
	
?>