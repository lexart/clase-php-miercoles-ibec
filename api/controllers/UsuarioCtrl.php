<?php 
	$objCelular = new Usuario();
	$results 	= array();

	if($ruteo == "celulares"){
		$results 	= $objCelular->getAllCelularItems($conn);
	}

	if($ruteo == "usuarios_grid"){
		$results 	= $objCelular->getAllUsuariosItemsGrid($conn);
	}

	if($ruteo == "celular_by_id"){
		$id = 0;
		if(isset($_GET) && !empty($_GET['id'])){
			$id = $_GET['id'];
		}
		if($id){
			$results 	= $objCelular->getCelularById($conn,$id);
			if(empty($results)){
				$results = array("err" => "not found");
			}
		} else {
			$results 	= array("err" => "not found");
		}
	}

	if($ruteo == "find_user"){
		$name = "";
		if(isset($_GET) && !empty($_GET['name'])){
			$name = $_GET['name'];
		}
		if($name){
			$results 	= $objCelular->findUserByName($conn,$name);
			if(empty($results)){
				$results = array("err" => "not found");
			}
		} else {
			$results 	= array("err" => "not found");
		}
	}

	if($ruteo == "celular_post"){
		$post 		= json_decode(file_get_contents('php://input'), true);
		$results 	= $objCelular->insertNewCelular($conn, $post);
	}

	if($ruteo == "login"){
		$post 		= json_decode(file_get_contents('php://input'), true);
		$results 	= $objCelular->login($conn, $post["email"], $post["clave"]);
	}

	if($ruteo == "celular_edit"){
		$post 		= json_decode(file_get_contents('php://input'), true);
		$results 	= $objCelular->updateCelularById($conn, $post);
	}
	
?>