<?php

ini_set('display_errors', 'Off');
error_reporting(0);

Class Connection {

	private $conexion;

	public function __construct(){
		$servidor = "localhost:3307";
		$usuario  = "root";
		$clave    = "";
		$base	  = "tienda";
		$this->conexion = mysqli_connect($servidor, $usuario, $clave, $base)
			or die('No se pudo conectar: ' . mysql_error());
		mysqli_set_charset($this->conexion, 'utf8');
		//mysql_select_db($base, $this->conexion) or die('No se pudo seleccionar la base de datos');
	}

	public function __destruct(){

	}

	public function query($sql){
		$resultado = mysqli_query($this->conexion, $sql) or die('Consulta fallida: ' . mysql_error());
		$datos = "";
		//guardamos en un array
		while($fila =  mysqli_fetch_array($resultado, MYSQLI_ASSOC)){
			$datos[] = $fila;
		}    	
		// Liberar resultados
		mysqli_free_result($resultado);

		return $datos;
	}

	public function close(){
		mysql_close($this->conexion);
	}

}
?>