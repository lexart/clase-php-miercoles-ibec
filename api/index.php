<?php 
	require('config/conn.php');
	$conn = new Connection();

	// RUTEO
	require('utils/route.php');

	if($exist){
		require('classes/'.$class.'.php');
		require('controllers/'.$class.'Ctrl.php');
	} else {
		$results = array("err" => "not found");
	}
	
	// $results = $conn->query("SELECT * FROM celulares");
	
	header('Content-Type: application/json');
	echo json_encode($results);
?>