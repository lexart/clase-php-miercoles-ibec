<p><?php echo $title; ?></p>
<?php if($displayCreate){ ?>
<form action="<?php echo $urlCtrl . 'ingresarCelularCtrl.php'; ?>" method="POST">
	<input type="text" class="form-control" name="marca" placeholder="Marca" required>
	<br>
	<input type="text" class="form-control" name="modelo" placeholder="Modelo" required>
	<br>
	<input type="number" class="form-control" name="precio" placeholder="Precio" required>
	<br>
	<?php if(!empty($msg)){ ?>
		<div class="alert alert-success"><?php echo $msg; ?></div>
	<?php } ?>
	<button class="btn btn-danger" type="reset">Cancelar</button>
	<button class="btn btn-primary" type="submit">Agregar producto</button> 
</form>
<?php } ?>
<?php if($displayTable){ ?>
	<a class="btn btn-primary" href="?ruteo=celulares&crear">Crear celular</a>
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Modelo</th>
				<th>Marca</th>
				<th>Precio</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		<?php for ($i=0; $i < count($celulares) ; $i++) { ?>
			<tr>
				<td><?php echo $celulares[$i]->id; ?></td>
				<td><?php echo $celulares[$i]->modelo; ?></td>
				<td><?php echo $celulares[$i]->marca; ?></td>
				<td><?php echo $celulares[$i]->precio; ?></td>
				<td>
					<a href="?ruteo=celulares&id=<?php echo $celulares[$i]->id; ?>" class="btn btn-primary">Editar</a>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
<?php } ?>

<?php if($displayEdit){ ?>
<form action="<?php echo $urlCtrl . 'ingresarCelularCtrl.php'; ?>" method="POST">
	<input type="hidden" name="id" value="<?php echo $celular->id; ?>">
	<input type="text" value="<?php echo $celular->marca; ?>" class="form-control" name="marca" placeholder="Marca" required>
	<br>
	<input type="text" value="<?php echo $celular->modelo; ?>" class="form-control" name="modelo" placeholder="Modelo" required>
	<br>
	<input type="number" value="<?php echo $celular->precio; ?>" class="form-control" name="precio" placeholder="Precio" required>
	<br>
	<?php if(!empty($msg)){ ?>
		<div class="alert alert-success"><?php echo $msg; ?></div>
	<?php } ?>
	<button class="btn btn-danger" type="reset">Cancelar</button>
	<button class="btn btn-primary" type="submit">Agregar producto</button> 
</form>
<?php } ?>