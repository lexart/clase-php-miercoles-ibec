<?php
	
	$title 		= "Celulares";
	$urlCtrl   	= "components/celulares/controllers/";
	$msg 		= "";

	$model 		= "celulares_grid";
	$celulares  = json_decode( file_get_contents( API . $model ) );

	// CONTROL DISPLAY VIEW
	$displayTable = true;
	$displayEdit  = false;
	$displayCreate= false;

	if(isset($_GET['msg'])){
		$msg = $_GET['msg'];
	}

	if(isset($_GET['id'])){
		$id = $_GET['id'];

		$model 			= "celular_by_id&id=".$id;
		$celular 		= json_decode( file_get_contents( API . $model ) );

		$displayTable 	= false;
		$displayEdit  	= true;
	}

	if(isset($_GET['crear'])){
		$displayCreate 	= true;
		$displayTable 	= false;
	}

?>