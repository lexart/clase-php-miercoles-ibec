<?php 
	define('API', 'http://localhost:8080/clase-php-miercoles-ibec/api/?ruteo=');
	define('APP', 'http://localhost:8080/clase-php-miercoles-ibec/?ruteo=');

	$celular = $_POST;

	if(empty($celular["id"])){
		$ch = curl_init(API . 'celular_post'); 
	} else {
		$ch = curl_init(API . 'celular_edit'); 
	}

	$postData = json_encode($celular);
	                                                                     
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));                                                                                                                   
	                                                                                                                     
	$result = json_decode( curl_exec($ch) );
	if(!empty($celular["id"])){
		header('location: '. APP . 'celulares&id='.$celular["id"].'&msg='.$result->success);
	} else {
		header('location: '. APP . 'celulares&msg='.$result->success."&crear");
	}
?>