# README #
# CLONAR PROYECTO 

- git clone URL

# CADA VEZ QUE ENTRO A LA CARPETA CHEQUEAR CAMBIOS

- git fetch
- git pull

# VERIFICAR CAMBIOS

- git status

# PREPARAR PARA SUBIR A REPO

- git add .
- git commit . -m "mi commit"
- git push

# CREAR BRANCH

- git checkout -b mi-branch

# VOLVER A MASTER ( O MOVERME POR CUALQUIER RAMA )

- git checkout nombre-rama

# MERGEAR CAMBIOS DESDE UNA RAMA
# ESTOY PARADO EN env-clase y quiero mergear con master

- git merge master

# CONFIGURAR DATOS DEL GIT

- git config user.email "mi@email.com"
- git config user.name "mi nombre"

(usar --global si quiero propagar el cambio en la raiz de mi entorno)